#!/bin/sh -eux

# Install with my local apt cacher
sudo echo 'Acquire::http::Proxy "http://192.168.0.4:3142/";' >> /etc/apt/apt.conf.d/02proxy

apt-get install -y openjdk-8-jdk-headless

# remove the cacher config
sudo rm -rf /etc/apt/apt.conf.d/02proxy
