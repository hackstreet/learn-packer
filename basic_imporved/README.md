# Imporved version of basic packer example #

This packer example fixes and implement all the todo points listed on the basic packer example.

I did get a lot done in this version, but the biggest problem is, I'm getting boxes of size ~1G which is absolutly unacceptable. For the next version I'm just going to copy the build scripts from the bento repo.


## Files ##

There are two dirs, stage1 where I convert the ubuntu server iso into an ovf and stage2 where I use the ovf to build a vagrant box with java installed.

### Stage1 ###

 1. `stage1/stage1_packer.json`: Packer build file for stage1.
 2. `stage1/http/preseed.cfg`: preseed file for automatic ubuntu install.

### Stage2 ###

 1. `stage2/stage2_packer.json`: Packer build file for stage2.
 2. `stage2/scripts/apt.sh`: All apt related activites. Update apt, upgrade the guest, install required packages etc.
 3. `stage2/scripts/vboxmount.sh`: Install the virtualbox extensions in the guest.
 4. `stage2/scripts/cleanup.sh`: Do final cleanup before creating the vagrant box.

## Todo ##

 Though every thing works, the size of the vagrant box build is 971MB which is way to large. So, I just going to copy the packer scripts from the bento repo and customize to my needs.

## Refrences ##

 1. Bento Repo

    https://github.com/chef/bento
