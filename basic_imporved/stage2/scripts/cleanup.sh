#!/bin/bash

# Apt cleanup.

apt autoremove
apt update
apt clean

# Delete unneeded files.
rm -f /home/vagrant/*.sh

# Zero out the rest of the free space using dd, then delete the written file.
dd if=/dev/zero of=/emptyfile bs=1M
rm -f /emptyfile

# Add `sync` so Packer doesn't quit too early, before the large file is deleted.
sync
