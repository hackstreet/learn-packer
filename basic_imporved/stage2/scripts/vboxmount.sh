#!/bin/bash


# Set up virtualbox guest additions, primarilly for mounting dirs

set -e


VBOX_VERSION="5.0.24"

sudo mount -o loop,ro ~/VBoxGuestAdditions_$VBOX_VERSION.iso /mnt/
sudo /mnt/VBoxLinuxAdditions.run || :
sudo umount /mnt/
rm -f ~/VBoxGuestAdditions_$VBOX_VERSION.iso

