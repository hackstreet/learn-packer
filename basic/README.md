# My First Attempt with Packer #

This was my first attempt at using packer to build a virtual box image. I was able to build a .box with java installed. I learnt a lot and the main point are in the gotcha section of this document.

## Files ##

 1. `basic.json` the full packer build file.
 2. `scripts/root_setup.sh` most of the provisioning.
 3. `scripts/vboxmount.sh` install the virtualbox extensions onto the Guest.
 4. `http/preseed.cfg` ubuntu preseed file.

## Gotchas ##


## Todo ##
 1. Try to break up the build into two parts during script development. Build ova first and and then
 build the .box. The reason for this is,
 `apt update` and `apt upgrade` take a lot of time to run on each iteration.
 Better to do the initall apt work once, while building the ova. Then I can use the ova to build
 the box, without repeating the apt setup steps.

 2. Work of an apt chache like apt-cacher-ng to cut out repeated network lookups.

 3. Upload to atlas

 4. Use a 'DEVELOP' variable to indicate that apt should be via a cacher and
    other time saving shortcuts.

 5. Better organize the scripts folder.

 6. compress the output vagrant box.

## Refrences ##

 1. Primary source of examples

    https://github.com/kaorimatz/packer-templates

 2. And the packer docs

    https://www.packer.io/docs/index.html

 3. A slightly outdated but really good introduction to packer

    https://blog.codeship.com/packer-vagrant-tutorial/
