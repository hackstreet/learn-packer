#!/bin/bash


# Set up virtualbox guest additions, primarilly for mounting dirs

set -e

sudo apt-get -y install dkms
sudo apt-get -y install make

sudo mount -o loop,ro ~/VBoxGuestAdditions_5.0.24.iso /mnt/
sudo /mnt/VBoxLinuxAdditions.run || :
sudo umount /mnt/
rm -f ~/VBoxGuestAdditions_5.0.24.iso
